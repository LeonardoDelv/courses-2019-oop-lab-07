package it.unibo.oop.lab.nesting2;

import java.util.Iterator;
import java.util.List;

import it.unibo.oop.lab.nesting2.Acceptor.ElementNotAcceptedException;
import it.unibo.oop.lab.nesting2.Acceptor.EndNotAcceptedException;

public class OneListAcceptable<T> implements Acceptable<T> {
	
	private final List<T> list;
	
	public OneListAcceptable(final List<T> l) {
		this.list = l;
	}
	
	@Override
	public Acceptor<T> acceptor() {
		final Iterator<T> iterator = list.iterator();
		return new Acceptor<T>() {
			
			/**
		     * Accept a new element. In case the element is not part of the set
		     * sequence, throws a {@link Acceptor.ElementNotAcceptedException}
		     * .
		     * 
		     * @param newElement
		     *            the next element to be accepted
		     * @throws ElementNotAcceptedException
		     *             if the element is out of the scheduled sequence
		     */
			@Override
		    public void accept(T newElement) throws Acceptor.ElementNotAcceptedException {
		    	try {
		    		if(newElement.equals(iterator.next())) {
		    			return;
		    		}
		    	} catch (Exception e) {
		    		System.out.println("no more element to be evaluated");
		    	}
		    	throw new Acceptor.ElementNotAcceptedException(newElement);
		    }
			
			/**
		     * Terminate to input new elements. If more elements have still to be
		     * inserted, throws an {@link Acceptor.EndNotAcceptedException}.
		     * 
		     * @throws EndNotAcceptedException
		     *             if more elements still need to be accepted
		     */
			@Override
			public void end() throws EndNotAcceptedException {
				try {
					if(!iterator.hasNext()) {
						return;
					}
				} catch(Exception e) {
					System.out.println("2" + e.getMessage());
				}
				throw new Acceptor.EndNotAcceptedException();
			}
		};
	}

}
